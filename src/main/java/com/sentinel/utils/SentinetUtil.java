package com.sentinel.utils;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRuleManager;

public class SentinetUtil {
	
	public static void initFlowRules() {
		FlowRule flowRule = new FlowRule();
		flowRule.setResource("testSentinet");
		flowRule.setGrade(RuleConstant.FLOW_GRADE_QPS);
		
		// set limit QPS to num  控制每秒多少请求
		flowRule.setCount(1);
		
		FlowRule flowRule_ = new FlowRule();
		flowRule_.setResource("annotationTest");
		flowRule_.setGrade(RuleConstant.FLOW_GRADE_QPS);
		flowRule_.setCount(1);
		
		
		List<FlowRule> rules = new ArrayList<>();
		rules.add(flowRule);
		rules.add(flowRule_);
		FlowRuleManager.loadRules(rules);
	}

}
