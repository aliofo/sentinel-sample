package com.sentinel.sample.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.csp.sentinel.Entry;
import com.alibaba.csp.sentinel.SphO;
import com.alibaba.csp.sentinel.SphU;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.sentinel.sample.base.controller.BaseController;

@RestController
public class SentinelController extends BaseController {

	@GetMapping("testSentinet")
	public String testSentinet() {
		testSentinetControl();
		testSentinetControl1();
		return "testSentinet";
	}

	public void testSentinetControl() {
		Entry entry = null;
		try {
			entry = SphU.entry("testSentinet");
			logger.info("testSentinet testSentinet testSentinet");

		} catch (BlockException e) {
			logger.error("blocked!!!!");
		} finally {
			if (entry != null) {
				entry.exit();
			}
		}
	}

	public void testSentinetControl1() {
		if (SphO.entry("testSentinet")) {
			// 定义被保护的资源
			logger.info("资源被访问！！");
			SphO.exit();
		} else {
			// 资源访问被阻止 被降级或限流
			// 进行相应的处理操作
			logger.info("资源访问被阻止！！");
		}
	}
}
