package com.sentinel.sample.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.sentinel.sample.base.controller.BaseController;

/**
 * 注解形式
 *
 */
@RestController
public class SentinelAnnotationController extends BaseController {

	@GetMapping("sentinel/annotation")
	public String testSentinetAnnotation() {
		annotationTest("测试");
		return "testSentinet";
	}
	
	@SentinelResource(value = "annotationTest", blockHandler = "exceptionHandler")
	public String annotationTest(String str) {
		logger.info(str);
		return "";
	}

	// Block 异常处理函数，参数最后多一个 BlockException，其余与原函数一致.
	public String exceptionHandler(String str, BlockException ex) {
		logger.error("Oops, error occurred at " + ex);
		return "Oops, error occurred at " + str;
	}

	// Fallback 函数，函数签名与原函数一致.
	public String helloFallback(String str) {
		return String.format("Halooooo %d", str);
	}
}
