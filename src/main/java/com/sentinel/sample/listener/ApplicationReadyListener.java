package com.sentinel.sample.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.boot.context.event.ApplicationPreparedEvent;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.ContextStoppedEvent;

import com.sentinel.sample.server.TestServer;
import com.sentinel.utils.SentinetUtil;

public class ApplicationReadyListener implements ApplicationListener<ApplicationEvent> {
	
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public void onApplicationEvent(ApplicationEvent e) {
		if(e instanceof ApplicationEnvironmentPreparedEvent) {
			logger.info("------------------------ApplicationEnvironmentPreparedEvent---------------------------------------");
		} else if(e instanceof ApplicationPreparedEvent) {
			logger.info("------------------------ApplicationPreparedEvent---------------------------------------");
		} else if(e instanceof ContextRefreshedEvent) {
			logger.info("------------------------ContextRefreshedEvent---------------------------------------");
		} else if(e instanceof ApplicationReadyEvent) {
			logger.info("------------------------ApplicationReadyEvent---------------------------------------");
			//SpringApplication springApplication = ((ApplicationReadyEvent)e).getSpringApplication();
			ApplicationContext applicationContext = ((ApplicationReadyEvent)e).getApplicationContext();
			TestServer test = applicationContext.getBean(TestServer.class);
			logger.info("------------------------get test bean---------------------------------------" + test.testMsg());
			
			// 定义规则
			logger.info("----------------------------------------------start initFlowRules---------------------------");
			SentinetUtil.initFlowRules();
			logger.info("----------------------------------------------end   initFlowRules---------------------------");
			
		} else if(e instanceof ContextStartedEvent) {
			logger.info("------------------------ContextStartedEvent---------------------------------------");
		} else if(e instanceof ContextStoppedEvent) {
			logger.info("------------------------ContextStoppedEvent---------------------------------------");
		} else if(e instanceof ContextClosedEvent) {
			logger.info("------------------------ContextClosedEvent---------------------------------------");
		}
	}
}
