package com.sentinel.sample.listener;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.stereotype.Component;

@Component
public class BizServerListener extends ApplicationReadyListener {
	
	public void onApplicationEvent(ApplicationReadyEvent e) {
		super.onApplicationEvent(e);
	}
}
