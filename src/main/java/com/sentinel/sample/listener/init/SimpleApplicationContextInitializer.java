package com.sentinel.sample.listener.init;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

public class SimpleApplicationContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

	@Override
	public void initialize(ConfigurableApplicationContext applicationContext) {
		/*if (applicationContext instanceof AnnotationConfigEmbeddedWebApplicationContext) {
			
			((AnnotationConfigEmbeddedWebApplicationContext) applicationContext).getBeanFactory().registerSingleton("testBean", new SimpleBean("id-001", "created by initializer"));
		}*/
		System.out.println("--------ApplicationContextInitializer--------");
	}

}
