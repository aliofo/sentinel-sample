package com.sentinel.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class SentinelSampleApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(SentinelSampleApplication.class, args);
	}

}

